﻿using System;
using Dolly.Abstractions;
using NLog;

namespace Dolly.Loggers.Nlog
{
    public class NlogLogger : IDollyLogger
    {
        private readonly Logger logger;

        public NlogLogger()
        {
            logger = LogManager.GetCurrentClassLogger();
        }

        public void Debug(string message)
        {
            logger.Debug(message);
        }

        public void Info(string message)
        {
            logger.Info(message);
        }

        public void Warn(string message)
        {
            logger.Warn(message);
        }

        public void Error(string message, Exception exception = null)
        {
            logger.Error(exception, message);
        }
    }
}