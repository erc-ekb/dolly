﻿using System;
using System.Linq;
using Dolly.DatabaseNamingStrategy;
using Dolly.Loggers;
using Dolly.ValueTypes;
using Microsoft.SqlServer.Management.Smo;
using NUnit.Framework;

namespace Dolly.Tests
{
    [TestFixture]
    public class CloneSchemaRunner
    {
        private const string LocalhostServer = "localhost";
        private const string DefaultDatabaseFolder = "C:\\db";

        [Test]
        [Ignore("only for manual start")]
        public void DeleteDateTimeBasedDatabases()
        {
            var server = new Server(LocalhostServer);
            var databaseNames = server.Databases.OfType<Database>()
                .Where(x => x.Name.Contains(DateTimeBasedDatabaseNamingStrategy.TempDatabaseSeparator))
                .Select(x => x.Name)
                .ToArray();
            foreach (var databaseName in databaseNames)
            {
                server.KillDatabase(databaseName);
                Console.WriteLine($"dropped {databaseName}");
            }
        }

        [Test]
        [Ignore("only for manual start")]
        public void Test1()
        {
            var state = new DollyState();
            var targetDatabaseStorage = new LocalhostTargetDatabaseStorage(LocalhostServer, DefaultDatabaseFolder);
            var cloningFactory = new CloningFactory(
                new ConsoleLogger(),
                targetDatabaseStorage,
                new ScriptEditor(targetDatabaseStorage, state),
                state
            );
            var cloningDatabaseName = "GKU";
            var sourceDatabaseInfo = new DatabaseInfo("srv-bash", cloningDatabaseName);
            var targetDatabaseInfo = new TargetDatabaseInfo(
                LocalhostServer,
                new DateTimeBasedDatabaseNamingStrategy(cloningDatabaseName),
                DefaultDatabaseFolder
            );
            var newDb = cloningFactory.CloneDatabaseSchema(sourceDatabaseInfo);
            cloningFactory.CloneViewWithDependencies(sourceDatabaseInfo, newDb, new DatabaseObjectInfo("vwded"));
        }
    }
}