﻿using System.IO;
using System.Reflection;

namespace Dolly.Tests
{
    public static class TestCaseFile
    {
        public static string GetString(string folderName, string fileName)
        {
            var filePath = GetFilePath(folderName, fileName);
            return File.ReadAllText(filePath);
        }

        public static string GetFilePath(string folderName, string fileName)
        {
            var root = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase).Replace("file:\\", "");
            return Path.Combine(root, "..", "..", "..", folderName, fileName);
        }
    }
}