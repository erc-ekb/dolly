﻿using System.Collections.Generic;
using ApprovalTests;
using ApprovalTests.Namers;
using ApprovalTests.Reporters;
using Dolly.Abstractions;
using Dolly.ValueTypes;
using NSubstitute;
using NUnit.Framework;

namespace Dolly.Tests.ScriptEditorTests
{
    [UseReporter(typeof(DiffReporter))]
    [UseApprovalSubdirectory(TestCasesFilesSubdirectoryName)]
    [TestFixture]
    public class ScriptEditorTests
    {
        private IScriptEditor scriptEditor;
        private IState state;

        public const string TestCasesFilesSubdirectoryName = "TestCases";
        public const string TestCasesFilesDirectoryName = "ScriptEditorTests\\" + TestCasesFilesSubdirectoryName;

        [SetUp]
        public void SetUp()
        {
            state = Substitute.For<IState>();
            scriptEditor = new ScriptEditor(new LocalhostTargetDatabaseStorage("localhost", "c:\\db"), state);
        }

        [Test]
        public void FixCreateDatabaseScript()
        {
            var script = TestCaseFile.GetString(TestCasesFilesDirectoryName, "ScriptEditorTests.FixCreateDatabaseScript.input.txt");

            var createDatabaseScript = scriptEditor.FixCreatingDatabaseScript(
                script,
                sourceDatabaseName: "GKU",
                targetDatabaseName: "New_Database_name",
                targetDatabaseFolderPath: "С:\\dbfolder");

            Approvals.Verify(createDatabaseScript);
        }

        [Test]
        public void FixCreateDatabaseScript_WithFilegrowthPercently()
        {
            var script = TestCaseFile.GetString(TestCasesFilesDirectoryName, "ScriptEditorTests.FixCreateDatabaseScript_WithFilegrowthPercently.input.txt");

            var createDatabaseScript = scriptEditor.FixCreatingDatabaseScript(
                script,
                sourceDatabaseName: "GKUBK",
                targetDatabaseName: "New_Database_name",
                targetDatabaseFolderPath: "С:\\dbfolder");

            Approvals.Verify(createDatabaseScript);
        }

        [Test]
        public void FixCreateView_WithExternalDatabases()
        {
            var script = TestCaseFile.GetString(TestCasesFilesDirectoryName, "ScriptEditorTests.FixCreateView_WithExternalDatabases.input.txt");

            var createdDatabases = new Dictionary<DatabaseInfo, string>
            {
                {new DatabaseInfo("localhost", "GKU"), "GKU_new"},
                {new DatabaseInfo("localhost", "GKU_2017"), "GKU_2017_new"},
                {new DatabaseInfo("localhost", "GKU_2016"), "GKU_2016_new"},
                {new DatabaseInfo("localhost", "GKU_2015"), "GKU_2015_new"},
                {new DatabaseInfo("localhost", "GKU_2005"), "GKU_2005_new"},
                {new DatabaseInfo("localhost", "GKUBK"), "GKUBK_new"}
            };
            state.GetCreatedDatabaseNames().Returns(createdDatabases);

            var createDatabaseScript = scriptEditor.FixCreatingScript(script);

            Approvals.Verify(createDatabaseScript);
        }

        [Test]
        public void FixCreateTable_WithColumnLikeDatabaseName()
        {
            var script = TestCaseFile.GetString(TestCasesFilesDirectoryName, "ScriptEditorTests.FixCreateTable_WithColumnLikeDatabaseName.input.txt");

            var createdDatabases = new Dictionary<DatabaseInfo, string>
            {
                {new DatabaseInfo("localhost", "TMP"), "TMP_new"}
            };
            state.GetCreatedDatabaseNames().Returns(createdDatabases);

            var createDatabaseScript = scriptEditor.FixCreatingScript(script);

            Approvals.Verify(createDatabaseScript); 
        }

        [Test]
        public void FixCreateView_WithDatabaseInLowercase()
        {
            var script = TestCaseFile.GetString(TestCasesFilesDirectoryName, "ScriptEditorTests.FixCreateView_WithDatabaseInLowercase.input.txt");

            var createdDatabases = new Dictionary<DatabaseInfo, string>
            {
                {new DatabaseInfo("localhost", "GKU"), "GKU_new"}
            };
            state.GetCreatedDatabaseNames().Returns(createdDatabases);

            var createDatabaseScript = scriptEditor.FixCreatingScript(script);

            Approvals.Verify(createDatabaseScript);
        }
    }
}