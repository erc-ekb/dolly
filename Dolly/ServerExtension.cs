﻿using System;
using Dolly.ValueTypes;
using Microsoft.SqlServer.Management.Smo;

namespace Dolly
{
    public static class ServerExtension
    {
        public static Database GetDatabase(this Server server, DatabaseInfo databaseInfo)
        {
            if (databaseInfo == null)
                throw new ArgumentNullException(nameof(databaseInfo));

            return server.GetDatabase(databaseInfo.DatabaseName);
        }

        public static Database GetDatabase(this Server server, TargetDatabaseInfo targetDatabaseInfo)
        {
            if (targetDatabaseInfo == null)
                throw new ArgumentNullException(nameof(targetDatabaseInfo));

            return server.GetDatabase(targetDatabaseInfo.DatabaseName);
        }

        public static Database GetDatabase(this Server server, string databaseName)
        {
            if (databaseName == null)
                throw new ArgumentNullException(nameof(databaseName));

            var database = server.Databases[databaseName];
            if (database == null)
                throw new Exception($"Database with name {databaseName} not found");

            return database;
        }
    }
}