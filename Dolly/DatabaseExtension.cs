﻿using System;
using System.Text;
using Dolly.Abstractions;
using Dolly.ValueTypes;
using JetBrains.Annotations;
using Microsoft.SqlServer.Management.Smo;

namespace Dolly
{
    public static class DatabaseExtension
    {
        internal static void ExecuteNonQueryWithLogger(this Database database, string sqlScript, IDollyLogger logger, string errorMessagePrefix = null)
        {
            try
            {
                database.ExecuteNonQuery(sqlScript);
            }
            catch (Exception e)
            {
                var errorMessageBuilder = new StringBuilder();
                if (errorMessagePrefix != null)
                    errorMessageBuilder.AppendLine(errorMessagePrefix);
                errorMessageBuilder.AppendLine($"Error on SQL script: {sqlScript}");
                logger.Error(errorMessageBuilder.ToString(), e);
            }
        }

        internal static DatabaseInfo ToDatabaseInfo(this Database database)
        {
            return new DatabaseInfo(database.Parent.Name, database.Name);
        }

        [NotNull]
        internal static Table GetTable(this Database database, DatabaseObjectInfo databaseObjectInfo)
        {
            return database.Get(database.Tables[databaseObjectInfo.Name, databaseObjectInfo.Schema], databaseObjectInfo);
        }

        [NotNull]
        internal static View GetView(this Database database, DatabaseObjectInfo databaseObjectInfo)
        {
            return database.Get(database.Views[databaseObjectInfo.Name, databaseObjectInfo.Schema], databaseObjectInfo);
        }

        [NotNull]
        private static T Get<T>(this Database database, T databaseObject, DatabaseObjectInfo databaseObjectInfo)
        {
            if (database == null)
                throw new ArgumentNullException(nameof(database));

            if (databaseObject == null)
                throw new Exception($"{typeof(T)} [{databaseObjectInfo.Schema}].[{databaseObjectInfo.Name}] on server {database.Parent.Name}  not found");

            return databaseObject;
        }
    }
}