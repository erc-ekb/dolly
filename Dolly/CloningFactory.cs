﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dolly.Abstractions;
using Dolly.ValueTypes;
using JetBrains.Annotations;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using Microsoft.SqlServer.Management.Smo;

namespace Dolly
{
    public class CloningFactory : ICloningFactory
    {
        private readonly IDollyLogger dollyLogger;
        private readonly ITargetDatabaseStorage targetDatabaseStorage;
        private readonly IScriptEditor scriptEditor;
        private readonly IState state;

        public CloningFactory(
            IDollyLogger dollyLogger,
            ITargetDatabaseStorage targetDatabaseStorage,
            IScriptEditor scriptEditor,
            IState state)
        {
            this.dollyLogger = dollyLogger;
            this.targetDatabaseStorage = targetDatabaseStorage;
            this.scriptEditor = scriptEditor;
            this.state = state;
        }

        public Database CloneDatabaseSchema(DatabaseInfo sourceDatabaseInfo)
        {
            if (sourceDatabaseInfo == null)
                throw new ArgumentNullException(nameof(sourceDatabaseInfo));

            var targetDatabaseInfo = targetDatabaseStorage.GetTargetDatabaseInfo(sourceDatabaseInfo.DatabaseName);
            dollyLogger.Info($"Creating database {targetDatabaseInfo.DatabaseName} to {targetDatabaseInfo.DatabaseFolderPath}");

            var sourceDatabaseServer = new Server(sourceDatabaseInfo.MsSqlInstance);
            var targetDatabaseServer = new Server(targetDatabaseInfo.MsSqlInstance);
            var sourceDatabase = sourceDatabaseServer.GetDatabase(sourceDatabaseInfo);

            var masterDatabase = targetDatabaseServer.GetDatabase("master");
            dollyLogger.Debug($"Found master database of server: {targetDatabaseServer}");
            var creatingDatabaseScripts = scriptEditor.GetCreatingObjectScripts(sourceDatabase);

            foreach (var script in creatingDatabaseScripts)
            {
                masterDatabase.ExecuteNonQueryWithLogger(script, dollyLogger);
            }

            dollyLogger.Info($"Created database {targetDatabaseInfo.DatabaseName}");
            var targetDatabase = targetDatabaseServer.GetDatabase(targetDatabaseInfo);
            if (targetDatabase == null)
                throw new Exception($"Database {targetDatabaseInfo.DatabaseName} not found");

            state.AddCreatedDatabase(sourceDatabaseInfo, targetDatabase);
            CloneDatabaseSchemas(sourceDatabase, targetDatabase);

            return targetDatabase;
        }

        public void CloneStoredProcedureWithDependencies(
            DatabaseInfo sourceDatabaseInfo,
            Database targetDatabase,
            DatabaseObjectInfo storedProcedureInfo)
        {
            if (sourceDatabaseInfo == null)
                throw new ArgumentNullException(nameof(sourceDatabaseInfo));
            if (targetDatabase == null)
                throw new ArgumentNullException(nameof(targetDatabase));
            if (storedProcedureInfo == null)
                throw new ArgumentNullException(nameof(storedProcedureInfo));
            var operation = $"clone stored procedure [{storedProcedureInfo.Schema}].[{storedProcedureInfo.Name}]";
            LogCloneOperationStarted(sourceDatabaseInfo, targetDatabase, operation);

            var sourceDatabaseServer = new Server(sourceDatabaseInfo.MsSqlInstance);
            var sourceDatabase = sourceDatabaseServer.GetDatabase(sourceDatabaseInfo);
            var storedProcedure = sourceDatabase.StoredProcedures[storedProcedureInfo.Name, storedProcedureInfo.Schema];
            DiscoverAndCreateDependencies(
                sourceDatabaseServer,
                sourceDatabase,
                targetDatabase,
                new[] { storedProcedure.Urn });

            LogCloneOperationCompleted(operation);
        }

        public void CloneAllStoredProceduresWithDependencies(DatabaseInfo sourceDatabaseInfo, Database targetDatabase)
        {
            if (sourceDatabaseInfo == null)
                throw new ArgumentNullException(nameof(sourceDatabaseInfo));
            if (targetDatabase == null)
                throw new ArgumentNullException(nameof(targetDatabase));
            var operation = "clone stored procedures";
            LogCloneOperationStarted(sourceDatabaseInfo, targetDatabase, operation);

            var sourceDatabaseServer = new Server(sourceDatabaseInfo.MsSqlInstance);
            var sourceDatabase = sourceDatabaseServer.GetDatabase(sourceDatabaseInfo);
            DiscoverAndCreateDependencies(
                sourceDatabaseServer,
                sourceDatabase,
                targetDatabase,
                sourceDatabase.StoredProcedures.OfType<StoredProcedure>().Select(x => x.Urn).ToArray());

            LogCloneOperationCompleted(operation);
        }

        public void CloneTableWithDependencies(
            DatabaseInfo sourceDatabaseInfo,
            Database targetDatabase,
            DatabaseObjectInfo tableInfo)
        {
            if (sourceDatabaseInfo == null)
                throw new ArgumentNullException(nameof(sourceDatabaseInfo));
            if (targetDatabase == null)
                throw new ArgumentNullException(nameof(targetDatabase));
            if (tableInfo == null)
                throw new ArgumentNullException(nameof(tableInfo));
            var operation = $"clone table [{tableInfo.Schema}].[{tableInfo.Name}]";
            LogCloneOperationStarted(sourceDatabaseInfo, targetDatabase, operation);

            var sourceDatabaseServer = new Server(sourceDatabaseInfo.MsSqlInstance);
            var sourceDatabase = sourceDatabaseServer.GetDatabase(sourceDatabaseInfo);
            var table = sourceDatabase.GetTable(tableInfo);
            DiscoverAndCreateDependencies(
                sourceDatabaseServer,
                sourceDatabase,
                targetDatabase,
                new[] { table.Urn });

            LogCloneOperationCompleted(operation);
        }

        public void CloneAllTablesWithDependencies(DatabaseInfo sourceDatabaseInfo, Database targetDatabase)
        {
            if (sourceDatabaseInfo == null)
                throw new ArgumentNullException(nameof(sourceDatabaseInfo));
            if (targetDatabase == null)
                throw new ArgumentNullException(nameof(targetDatabase));
            var operation = "clone tables";
            LogCloneOperationStarted(sourceDatabaseInfo, targetDatabase, operation);

            var sourceDatabaseServer = new Server(sourceDatabaseInfo.MsSqlInstance);
            var sourceDatabase = sourceDatabaseServer.GetDatabase(sourceDatabaseInfo);
            DiscoverAndCreateDependencies(
                sourceDatabaseServer,
                sourceDatabase,
                targetDatabase,
                sourceDatabase.Tables.OfType<Table>().Select(x => x.Urn).ToArray());

            LogCloneOperationCompleted(operation);
        }

        public void CloneViewWithDependencies(
            DatabaseInfo sourceDatabaseInfo,
            Database targetDatabase,
            DatabaseObjectInfo viewInfo)
        {
            if (sourceDatabaseInfo == null)
                throw new ArgumentNullException(nameof(sourceDatabaseInfo));
            if (targetDatabase == null)
                throw new ArgumentNullException(nameof(targetDatabase));
            if (viewInfo == null)
                throw new ArgumentNullException(nameof(viewInfo));
            var operation = $"clone view [{viewInfo.Schema}].[{viewInfo.Name}]";
            LogCloneOperationStarted(sourceDatabaseInfo, targetDatabase, operation);

            var sourceDatabaseServer = new Server(sourceDatabaseInfo.MsSqlInstance);
            var sourceDatabase = sourceDatabaseServer.GetDatabase(sourceDatabaseInfo);
            var view = sourceDatabase.GetView(viewInfo);
            DiscoverAndCreateDependencies(
                sourceDatabaseServer,
                sourceDatabase,
                targetDatabase,
                new[] { view.Urn });

            LogCloneOperationCompleted(operation);
        }

        public void CloneAllViewsWithDependencies(DatabaseInfo sourceDatabaseInfo, Database targetDatabase)
        {
            if (sourceDatabaseInfo == null)
                throw new ArgumentNullException(nameof(sourceDatabaseInfo));
            if (targetDatabase == null)
                throw new ArgumentNullException(nameof(targetDatabase));
            var operation = "clone tables";
            LogCloneOperationStarted(sourceDatabaseInfo, targetDatabase, operation);

            var sourceDatabaseServer = new Server(sourceDatabaseInfo.MsSqlInstance);
            var sourceDatabase = sourceDatabaseServer.GetDatabase(sourceDatabaseInfo);
            DiscoverAndCreateDependencies(
                sourceDatabaseServer,
                sourceDatabase,
                targetDatabase,
                sourceDatabase.Views.OfType<View>().Select(x => x.Urn).ToArray());

            LogCloneOperationCompleted(operation);
        }

        public void CloneAllWithDependencies(DatabaseInfo sourceDatabaseInfo, Database targetDatabase)
        {
            if (sourceDatabaseInfo == null)
                throw new ArgumentNullException(nameof(sourceDatabaseInfo));
            if (targetDatabase == null)
                throw new ArgumentNullException(nameof(targetDatabase));
            CloneAllTablesWithDependencies(sourceDatabaseInfo, targetDatabase);

            var operation = "clone stored procedures, views, functions";
            LogCloneOperationStarted(sourceDatabaseInfo, targetDatabase, operation);

            var sourceDatabaseServer = new Server(sourceDatabaseInfo.MsSqlInstance);
            var sourceDatabase = sourceDatabaseServer.GetDatabase(sourceDatabaseInfo);
            var urns = new List<Urn>();
            urns.AddRange(sourceDatabase.Views.OfType<View>().Select(x => x.Urn).ToArray());
            urns.AddRange(sourceDatabase.StoredProcedures.OfType<StoredProcedure>().Select(x => x.Urn).ToArray());
            urns.AddRange(sourceDatabase.UserDefinedFunctions.OfType<UserDefinedFunction>().Select(x => x.Urn).ToArray());

            DiscoverAndCreateDependencies(
                sourceDatabaseServer,
                sourceDatabase,
                targetDatabase,
                urns.ToArray());

            LogCloneOperationCompleted(operation);
        }

        private void CloneDatabaseSchemas(Database sourceDatabase, Database targetDatabase)
        {
            var operation = "clone database schemas";
            LogCloneOperationStarted(sourceDatabase, targetDatabase, operation);

            var schemas = sourceDatabase.Schemas;
            foreach (Schema schema in schemas)
            {
                CreateObject(sourceDatabase, targetDatabase, schema);
            }

            LogCloneOperationCompleted(operation);
        }

        private void LogCloneOperationStarted(Database sourceDatabase, Database targetDatabase, string operation)
        {
            LogCloneOperationStarted(sourceDatabase.Parent.Name, sourceDatabase.Name, targetDatabase, operation);
        }

        private void LogCloneOperationStarted(DatabaseInfo sourceDatabaseInfo, Database targetDatabase, string operation)
        {
            LogCloneOperationStarted(sourceDatabaseInfo.MsSqlInstance, sourceDatabaseInfo.DatabaseName, targetDatabase, operation);
        }

        private void LogCloneOperationStarted(string sourceDatabaseInstance, string sourceDatabaseName, Database targetDatabase, string operation)
        {
            dollyLogger.Info($"Starting operation: {operation}");
            dollyLogger.Info($"Source server instance: {sourceDatabaseInstance}, database: {sourceDatabaseName}");
            dollyLogger.Info($"Target server instance: {targetDatabase.Parent.Name}, database: {targetDatabase.Name}");
        }

        private void LogCloneOperationCompleted(string operation)
        {
            dollyLogger.Info($"Operation {operation} completed.");
        }

        private void DiscoverAndCreateDependencies(Server sourceDatabaseServer, Database sourceDatabase, Database targetDatabase, Urn[] urns)
        {
            var walker = new DependencyWalker(sourceDatabaseServer);

            dollyLogger.Debug($"Starting discover dependencies of {urns.Length} objects");
            var dependencyTree = walker.DiscoverDependencies(urns.ToArray(), DependencyType.Parents);
            var dependencyCollectionNodes = walker.WalkDependencies(dependencyTree).ToArray();

            DisableStrictSecurity(targetDatabase);
            foreach (var node in dependencyCollectionNodes)
            {
                CreateByUrn(node.Urn, sourceDatabase, targetDatabase);
            }

            EnableStrictSecurity(targetDatabase);
        }

        private void CreateByUrn(Urn urn, Database sourceDatabase, Database targetDatabase)
        {
            if (urn.Type == "UnresolvedEntity")
            {
                dollyLogger.Debug($"Skip unresolvedEntity with urn {urn}");
                return;
            }

            var creatingObjectUrnInfo = UrnInfo.FromUrn(urn);
            var name = creatingObjectUrnInfo.ObjectInfo.Name;
            var schema = creatingObjectUrnInfo.ObjectInfo.Schema;

            if (creatingObjectUrnInfo.DatabaseInfo != sourceDatabase.ToDatabaseInfo())
            {
                dollyLogger.Warn($"Попытка создать объект {urn}");

                CreateExternalObject(urn, creatingObjectUrnInfo);
            }

            switch (urn.Type)
            {
                case "UserDefinedFunction":
                    var definedFunction = ReturnObjectOrLogNotFound(sourceDatabase.UserDefinedFunctions[name, schema], urn);
                    if (definedFunction == null)
                        return;
                    CreateObject(sourceDatabase, targetDatabase, definedFunction);
                    break;
                case "View":
                    var view = ReturnObjectOrLogNotFound(sourceDatabase.Views[name, schema], urn);
                    if (view == null)
                        return;
                    CreateObject(sourceDatabase, targetDatabase, view);

                    foreach (Index viewIndex in view.Indexes)
                    {
                        CreateObject(sourceDatabase, targetDatabase, viewIndex);
                    }

                    break;
                case "StoredProcedure":
                    var storedProcedure = ReturnObjectOrLogNotFound(sourceDatabase.StoredProcedures[name, schema], urn);
                    if (storedProcedure == null)
                        return;
                    CreateObject(sourceDatabase, targetDatabase, storedProcedure);
                    break;
                case "Table":
                    var table = ReturnObjectOrLogNotFound(sourceDatabase.Tables[name, schema], urn);
                    if (table == null)
                        return;
                    CreateObject(sourceDatabase, targetDatabase, table);

                    foreach (Trigger tableTrigger in table.Triggers.OfType<Trigger>().Where(x => x.IsEnabled))
                    {
                        CreateObject(sourceDatabase, targetDatabase, tableTrigger);
                    }

                    break;
                case "SqlAssembly":
                    var sqlAssembly = ReturnObjectOrLogNotFound(sourceDatabase.Assemblies[name], urn);
                    if (sqlAssembly == null)
                        return;
                    CreateObject(sourceDatabase, targetDatabase, sqlAssembly);
                    break;
                case "UserDefinedTableType":
                    var userDefinedTableType = ReturnObjectOrLogNotFound(sourceDatabase.UserDefinedTableTypes[name, schema], urn);
                    if (userDefinedTableType == null)
                        return;
                    CreateObject(sourceDatabase, targetDatabase, userDefinedTableType);
                    break;
                case "UserDefinedAggregate":
                    var userDefinedAggregate = ReturnObjectOrLogNotFound(sourceDatabase.UserDefinedAggregates[name, schema], urn);
                    if (userDefinedAggregate == null)
                        return;
                    CreateObject(sourceDatabase, targetDatabase, userDefinedAggregate);
                    break;
                case "Sequence":
                    var sequence = ReturnObjectOrLogNotFound(sourceDatabase.Sequences[name, schema], urn);
                    if (sequence == null)
                        return;
                    CreateObject(sourceDatabase, targetDatabase, sequence);
                    break;
                case "Synonym":
                    var synonym = ReturnObjectOrLogNotFound(sourceDatabase.Synonyms[name, schema], urn);
                    if (synonym == null)
                        return;
                    CreateObject(sourceDatabase, targetDatabase, synonym);
                    break;
                case "XmlSchemaCollection":
                    var xmlSchemaCollection = ReturnObjectOrLogNotFound(sourceDatabase.XmlSchemaCollections[name, schema], urn);
                    if (xmlSchemaCollection == null)
                        return;
                    CreateObject(sourceDatabase, targetDatabase, xmlSchemaCollection);
                    break;
                case "UnresolvedEntity":
                    //no action
                    break;
                case "ExtendedProperty":
                    var extendedProperty = ReturnObjectOrLogNotFound(sourceDatabase.ExtendedProperties[name], urn);
                    if (extendedProperty == null)
                        return;
                    CreateObject(sourceDatabase, targetDatabase, extendedProperty);
                    break;
                case "Trigger":
                    var trigger = ReturnObjectOrLogNotFound(sourceDatabase.Triggers[name], urn);
                    if (trigger == null)
                        return;
                    CreateObject(sourceDatabase, targetDatabase, trigger);
                    break;
                default:
                    throw new NotSupportedException($"Not supported urn type: {urn}");
            }
        }

        private void CreateExternalObject(Urn urn, UrnInfo creatingObjectUrnInfo)
        {
            if (!state.TryGetCreatedDatabase(creatingObjectUrnInfo.DatabaseInfo, out var externalTargetDatabase))
            {
                externalTargetDatabase = CloneDatabaseSchema(creatingObjectUrnInfo.DatabaseInfo);
            }

            var externalServer = new Server(creatingObjectUrnInfo.DatabaseInfo.MsSqlInstance);
            var externalDatabase = externalServer.GetDatabase(creatingObjectUrnInfo.DatabaseInfo);

            switch (urn.Type)
            {
                case "Table":
                    CloneTableWithDependencies(externalDatabase.ToDatabaseInfo(), externalTargetDatabase, creatingObjectUrnInfo.ObjectInfo);
                    break;
                case "View":
                    CloneViewWithDependencies(externalDatabase.ToDatabaseInfo(), externalTargetDatabase, creatingObjectUrnInfo.ObjectInfo);
                    break;
                case "StoredProcedure":
                    CloneStoredProcedureWithDependencies(externalDatabase.ToDatabaseInfo(), externalTargetDatabase, creatingObjectUrnInfo.ObjectInfo);
                    break;
            }
        }

        private T ReturnObjectOrLogNotFound<T>(T item, Urn urn)
        {
            if (item == null)
                LogNotFoundObject(urn);
            return item;
        }

        private void LogNotFoundObject(Urn urn)
        {
            dollyLogger.Warn($"{urn} not found");
        }

        private void CreateObject<T>(Database sourceDatabase, Database targetDatabase, T creatingItem)
            where T : SqlSmoObject, IScriptable
        {
            if (IsSystemObject(creatingItem))
            {
                dollyLogger.Warn($"{creatingItem.Urn} is system object");
                return;
            }

            if (state.DatabaseObjectAlreadyCreated(creatingItem.Urn))
            {
                dollyLogger.Warn($"{creatingItem.Urn} already created");
                return;
            }

            dollyLogger.Debug($"Start cloning for: {creatingItem.Urn}");

            var scripts = scriptEditor.GetCreatingObjectScripts(creatingItem);
            foreach (var script in scripts)
            {
                targetDatabase.ExecuteNonQueryWithLogger(script, dollyLogger, $"Error create object {creatingItem}");
            }

            state.AddCreatedDatabaseObjectUrn(creatingItem.Urn);
            dollyLogger.Debug($"Created clone for: {creatingItem.Urn}");

            var extendedProperties = GetExtendedPropertyCollection(creatingItem);
            if (extendedProperties == null)
                return;

            foreach (ExtendedProperty extendedProperty in extendedProperties)
            {
                CreateObject(sourceDatabase, targetDatabase, extendedProperty);
            }
        }

        private void DisableStrictSecurity(Database targetDatabase)
        {
            var server = targetDatabase.Parent;
            if (server.Version.Major >= 14)
            {
                targetDatabase.ExecuteNonQueryWithLogger("EXEC sp_configure 'show advanced options', 1;", dollyLogger);
                targetDatabase.ExecuteNonQueryWithLogger("RECONFIGURE", dollyLogger);
                targetDatabase.ExecuteNonQueryWithLogger("EXEC sp_configure 'clr strict security', 0;", dollyLogger);
                targetDatabase.ExecuteNonQueryWithLogger("RECONFIGURE", dollyLogger);

                dollyLogger.Info("Disabled strict security");
            }
            else
            {
                dollyLogger.Debug($"Security not disabled for MSSQLServer version {server.Version.Major}");
            }
        }

        private void EnableStrictSecurity(Database targetDatabase)
        {
            var server = targetDatabase.Parent;
            if (server.Version.Major >= 14)
            {
                targetDatabase.ExecuteNonQueryWithLogger("EXEC sp_configure 'clr strict security', 1;", dollyLogger);
                targetDatabase.ExecuteNonQueryWithLogger("RECONFIGURE", dollyLogger);
                targetDatabase.ExecuteNonQueryWithLogger("EXEC sp_configure 'show advanced options', 0;", dollyLogger);
                targetDatabase.ExecuteNonQueryWithLogger("RECONFIGURE", dollyLogger);

                dollyLogger.Info("Enabled strict security");
            }
            else
            {
                dollyLogger.Debug($"Security not enabled for MSSQLServer version {server.Version.Major}");
            }
        }

        private bool IsSystemObject(object src)
        {
            var value = src.GetType().GetProperty("IsSystemObject")?.GetValue(src, null);
            return value != null && Convert.ToBoolean(value);
        }

        private ExtendedPropertyCollection GetExtendedPropertyCollection(object src)
        {
            var value = src.GetType().GetProperty("ExtendedProperties")?.GetValue(src, null);
            return value as ExtendedPropertyCollection;
        }

        public void TruncateStoredProcedureTableDependencies([NotNull] Database database, [NotNull] DatabaseObjectInfo storedProcedureInfo)
        {
            if (database == null)
                throw new ArgumentNullException(nameof(database));
            
            if (storedProcedureInfo == null)
                throw new ArgumentNullException(nameof(storedProcedureInfo));

            var operation = $"truncate table dependencies for stored procedure [{storedProcedureInfo.Schema}].[{storedProcedureInfo.Name}]";
            dollyLogger.Debug($"Start {operation}");

            var storedProcedure = database.StoredProcedures[storedProcedureInfo.Name, storedProcedureInfo.Schema];

            var walker = new DependencyWalker(database.Parent);            
            var dependencyTree = walker.DiscoverDependencies(new []{ storedProcedure.Urn }, DependencyType.Parents);
            var dependencyCollectionNodes = walker.WalkDependencies(dependencyTree).ToArray();
            
            foreach(var item in dependencyCollectionNodes)
            {
                if (item.Urn.Type != "Table")
                    continue;

                var info = DatabaseObjectInfo.FromUrn(item.Urn);
                var truncateSqlQuery = $"truncate table [{info.Schema}].[{info.Name}]";
                database.ExecuteNonQueryWithLogger(truncateSqlQuery, dollyLogger);
            }

            dollyLogger.Debug($"End {operation}");
        }
    }
}