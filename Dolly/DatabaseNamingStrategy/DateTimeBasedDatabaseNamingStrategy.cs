﻿using System;
using Dolly.Abstractions;

namespace Dolly.DatabaseNamingStrategy
{
    public class DateTimeBasedDatabaseNamingStrategy : IDatabaseNamingStrategy
    {
        public const char TempDatabaseSeparator = '_';
        private readonly string sourceDatabaseName;

        public DateTimeBasedDatabaseNamingStrategy(string sourceDatabaseName)
        {
            this.sourceDatabaseName = sourceDatabaseName;
        }

        public string GetDatabaseName()
        {
            return $"{sourceDatabaseName}{TempDatabaseSeparator}{DateTime.Now:yy.MM.dd_HH:mm}";
        }
    }
}