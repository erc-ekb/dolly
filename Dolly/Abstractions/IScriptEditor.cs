﻿using Microsoft.SqlServer.Management.Smo;

namespace Dolly.Abstractions
{
    public interface IScriptEditor
    {
        string[] GetCreatingObjectScripts<T>(T creatingObject)
            where T : SqlSmoObject, IScriptable;

        string FixCreatingDatabaseScript(string creatingDatabaseScript, string sourceDatabaseName, string targetDatabaseName, string targetDatabaseFolderPath);
        string FixCreatingScript(string creatingDatabaseObjectScript);
        string FixCreatingExtendedPropertyScript(string creatingExtendedPropertyScript);
    }
}