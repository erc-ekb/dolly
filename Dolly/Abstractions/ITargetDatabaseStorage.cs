﻿using Dolly.ValueTypes;

namespace Dolly.Abstractions
{
    public interface ITargetDatabaseStorage
    {
        TargetDatabaseInfo GetTargetDatabaseInfo(string sourceDatabaseName);
    }
}