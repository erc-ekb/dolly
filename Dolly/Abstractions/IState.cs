﻿using Dolly.ValueTypes;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using Microsoft.SqlServer.Management.Smo;

namespace Dolly.Abstractions
{
    public interface IState
    {
        void AddCreatedDatabase(DatabaseInfo databaseInfo, Database database);
        bool TryGetCreatedDatabase(DatabaseInfo databaseInfo, out Database database);
        System.Collections.Generic.IReadOnlyDictionary<DatabaseInfo, string> GetCreatedDatabaseNames();

        void AddCreatedDatabaseObjectUrn(Urn createdDatabaseObjectUrn);
        bool DatabaseObjectAlreadyCreated(Urn databaseObjectUrn);
    }
}