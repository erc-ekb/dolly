﻿using Dolly.ValueTypes;
using JetBrains.Annotations;
using Microsoft.SqlServer.Management.Smo;

namespace Dolly.Abstractions
{
    public interface ICloningFactory
    {
        Database CloneDatabaseSchema([NotNull] DatabaseInfo sourceDatabaseInfo);

        void CloneStoredProcedureWithDependencies([NotNull] DatabaseInfo sourceDatabaseInfo, [NotNull] Database targetDatabase, [NotNull] DatabaseObjectInfo storedProcedureInfo);
        void CloneAllStoredProceduresWithDependencies([NotNull] DatabaseInfo sourceDatabaseInfo, [NotNull] Database targetDatabase);

        void CloneTableWithDependencies([NotNull] DatabaseInfo sourceDatabaseInfo, [NotNull] Database targetDatabase, [NotNull] DatabaseObjectInfo tableInfo);
        void CloneAllTablesWithDependencies([NotNull] DatabaseInfo sourceDatabaseInfo, [NotNull] Database targetDatabase);

        void CloneViewWithDependencies([NotNull] DatabaseInfo sourceDatabaseInfo, [NotNull] Database targetDatabase, [NotNull] DatabaseObjectInfo viewInfo);
        void CloneAllViewsWithDependencies([NotNull] DatabaseInfo sourceDatabaseInfo, [NotNull] Database targetDatabase);

        void CloneAllWithDependencies([NotNull] DatabaseInfo sourceDatabaseInfo, [NotNull] Database targetDatabase);

        void TruncateStoredProcedureTableDependencies([NotNull] Database database, [NotNull] DatabaseObjectInfo storedProcedure);
    }
}