﻿using System;

namespace Dolly.Abstractions
{
    public interface IDollyLogger
    {
        void Debug(string message);
        void Info(string message);
        void Warn(string message);
        void Error(string message, Exception exception = null);
    }
}