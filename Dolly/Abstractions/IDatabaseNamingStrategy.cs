﻿namespace Dolly.Abstractions
{
    public interface IDatabaseNamingStrategy
    {
        string GetDatabaseName();
    }
}