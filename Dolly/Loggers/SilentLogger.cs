﻿using System;
using Dolly.Abstractions;

namespace Dolly.Loggers
{
    public class SilentLogger : IDollyLogger
    {
        public void Debug(string message)
        {
        }

        public void Info(string message)
        {
        }

        public void Warn(string message)
        {
        }

        public void Error(string message, Exception exception = null)
        {
        }
    }
}