﻿using System;
using System.Text;
using Dolly.Abstractions;

namespace Dolly.Loggers
{
    public class ConsoleLogger : IDollyLogger
    {
        public void Debug(string message)
        {
            WriteLineOnConsole("debug", message);
        }

        public void Info(string message)
        {
            WriteLineOnConsole("info", message);
        }

        public void Warn(string message)
        {
            WriteLineOnConsole("warn", message);
        }

        public void Error(string message, Exception exception = null)
        {
            var messageBuilder = new StringBuilder(message);
            if (exception != null)
                messageBuilder.AppendLine(exception.ToString());
            WriteLineOnConsole("error", messageBuilder.ToString());
        }

        private void WriteLineOnConsole(string level, string message)
        {
            Console.WriteLine($"{level.ToUpper()}: {message}");
        }
    }
}