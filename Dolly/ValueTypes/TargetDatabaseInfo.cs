﻿using System;
using System.IO;
using Dolly.Abstractions;
using JetBrains.Annotations;

namespace Dolly.ValueTypes
{
    public class TargetDatabaseInfo
    {
        public TargetDatabaseInfo(
            [NotNull] string msSqlInstance,
            [NotNull] IDatabaseNamingStrategy databaseNamingStrategy,
            [NotNull] string databaseFolderPath)
        {
            if (databaseNamingStrategy == null)
                throw new ArgumentNullException(nameof(databaseNamingStrategy));
            if (databaseFolderPath == null)
                throw new ArgumentNullException(nameof(databaseFolderPath));

            MsSqlInstance = msSqlInstance ?? throw new ArgumentNullException(nameof(msSqlInstance));
            DatabaseName = databaseNamingStrategy.GetDatabaseName();

            var dbFolder = CreateDatabaseFolder(databaseFolderPath);
            DatabaseFolderPath = dbFolder;
        }

        public string MsSqlInstance { get; }
        public string DatabaseName { get; }
        public string DatabaseFolderPath { get; }

        private static string GetFolderNameForDb(string dbName) =>
            dbName.Replace(":", "-");

        private string CreateDatabaseFolder(string databaseFolderPath)
        {
            var dbFolder = databaseFolderPath;
            dbFolder = Path.Combine(dbFolder, GetFolderNameForDb(DatabaseName));
            if (!Directory.Exists(dbFolder))
                Directory.CreateDirectory(dbFolder);
            return dbFolder;
        }
    }
}