﻿using System;
using JetBrains.Annotations;
using Microsoft.SqlServer.Management.Sdk.Sfc;

namespace Dolly.ValueTypes
{
    public class DatabaseInfo
    {
        public DatabaseInfo([NotNull] string msSqlInstance, [CanBeNull] string databaseName)
        {
            MsSqlInstance = msSqlInstance ?? throw new ArgumentNullException(nameof(msSqlInstance));
            DatabaseName = databaseName ?? throw new ArgumentNullException(nameof(databaseName));
        }

        public static DatabaseInfo FromUrn(Urn urn)
        {
            var database = urn.XPathExpression.GetAttribute("Name", "Database");
            var server = urn.XPathExpression.GetAttribute("Name", "Server");

            var databaseInfo = new DatabaseInfo(server, database);
            return databaseInfo;
        }

        public string MsSqlInstance { get; }
        public string DatabaseName { get; }

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = 17;
                // Maybe nullity checks, if these are objects not primitives!
                hash = hash * 23 + MsSqlInstance.ToLower().GetHashCode();
                hash = hash * 23 + DatabaseName.ToLower().GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            var o = (DatabaseInfo)obj;
            return o.GetHashCode() == GetHashCode();
        }

        public static bool operator==(DatabaseInfo obj1, DatabaseInfo obj2)
        {
            if (ReferenceEquals(obj1, obj2))
            {
                return true;
            }

            if (ReferenceEquals(obj1, null))
            {
                return false;
            }

            if (ReferenceEquals(obj2, null))
            {
                return false;
            }

            return obj1.GetHashCode() == obj2.GetHashCode();
        }

        // this is second one '!='
        public static bool operator!=(DatabaseInfo obj1, DatabaseInfo obj2)
        {
            return !(obj1 == obj2);
        }
    }
}