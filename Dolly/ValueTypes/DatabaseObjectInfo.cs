﻿using System;
using JetBrains.Annotations;
using Microsoft.SqlServer.Management.Sdk.Sfc;

namespace Dolly.ValueTypes
{
    public class DatabaseObjectInfo
    {
        public DatabaseObjectInfo([NotNull] string name)
            : this("dbo", name)
        {
        }

        public DatabaseObjectInfo([CanBeNull] string schema, [NotNull] string name)
        {
            Schema = schema;
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public static DatabaseObjectInfo FromUrn(Urn urn)
        {
            var name = urn.XPathExpression.GetAttribute("Name", urn.Type);
            var schema = urn.XPathExpression.GetAttribute("Schema", urn.Type);
            return new DatabaseObjectInfo(schema, name);
        }

        [CanBeNull]
        public string Schema { get; }

        [NotNull]
        public string Name { get; }
    }
}