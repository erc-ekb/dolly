﻿using Microsoft.SqlServer.Management.Sdk.Sfc;

namespace Dolly.ValueTypes
{
    public class UrnInfo
    {
        public static UrnInfo FromUrn(Urn urn)
        {
            var databaseInfo = DatabaseInfo.FromUrn(urn);
            var objectInfo = DatabaseObjectInfo.FromUrn(urn);

            return new UrnInfo
            {
                DatabaseInfo = databaseInfo,
                ObjectInfo = objectInfo
            };
        }

        public DatabaseInfo DatabaseInfo { get; private set; }
        public DatabaseObjectInfo ObjectInfo { get; private set; }
    }
}