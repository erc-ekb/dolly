﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dolly.Abstractions;
using Dolly.ValueTypes;
using JetBrains.Annotations;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using Microsoft.SqlServer.Management.Smo;

namespace Dolly
{
    public class DollyState : IState
    {
        private readonly Dictionary<DatabaseInfo, Database> createdDatabases;
        private readonly HashSet<string> createdObjects;

        public DollyState()
        {
            createdDatabases = new Dictionary<DatabaseInfo, Database>();
            createdObjects = new HashSet<string>();
        }

        public void AddCreatedDatabase([NotNull] DatabaseInfo databaseInfo, [NotNull] Database database)
        {
            if (databaseInfo == null)
                throw new ArgumentNullException(nameof(databaseInfo));
            if (database == null)
                throw new ArgumentNullException(nameof(database));
            createdDatabases.Add(databaseInfo, database);
        }

        public bool TryGetCreatedDatabase([NotNull] DatabaseInfo databaseInfo, out Database database)
        {
            if (databaseInfo == null)
                throw new ArgumentNullException(nameof(databaseInfo));
            return createdDatabases.TryGetValue(databaseInfo, out database);
        }

        public System.Collections.Generic.IReadOnlyDictionary<DatabaseInfo, string> GetCreatedDatabaseNames() =>
            createdDatabases.ToDictionary(x => x.Key, x => x.Value.Name);

        public System.Collections.Generic.IReadOnlyDictionary<DatabaseInfo, Database> GetCreatedDatabases() =>
            createdDatabases;

        public void AddCreatedDatabaseObjectUrn([NotNull] Urn createdDatabaseObjectUrn)
        {
            if (createdDatabaseObjectUrn == null)
                throw new ArgumentNullException(nameof(createdDatabaseObjectUrn));
            createdObjects.Add(createdDatabaseObjectUrn.ToString().ToLower());
        }

        public bool DatabaseObjectAlreadyCreated([NotNull] Urn databaseObjectUrn)
        {
            if (databaseObjectUrn == null)
                throw new ArgumentNullException(nameof(databaseObjectUrn));
            return createdObjects.Contains(databaseObjectUrn.ToString().ToLower());
        }
    }
}