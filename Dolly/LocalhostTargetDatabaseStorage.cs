﻿using Dolly.Abstractions;
using Dolly.DatabaseNamingStrategy;
using Dolly.ValueTypes;
using JetBrains.Annotations;

namespace Dolly
{
    public class LocalhostTargetDatabaseStorage : ITargetDatabaseStorage
    {
        [NotNull]
        private readonly string msSqlInstance;
        [NotNull]
        private readonly string databaseFolderPath;

        public LocalhostTargetDatabaseStorage(
            [NotNull] string msSqlInstance,
            [NotNull] string databaseFolderPath)
        {
            this.msSqlInstance = msSqlInstance;
            this.databaseFolderPath = databaseFolderPath;
        }

        public TargetDatabaseInfo GetTargetDatabaseInfo(string sourceDatabaseName)
        {
            return new TargetDatabaseInfo(msSqlInstance, new DateTimeBasedDatabaseNamingStrategy(sourceDatabaseName), databaseFolderPath);
        }
    }
}