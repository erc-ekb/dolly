﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Dolly.Abstractions;
using Dolly.ValueTypes;
using Microsoft.SqlServer.Management.Smo;

namespace Dolly
{
    public class ScriptEditor : IScriptEditor
    {
        private readonly ITargetDatabaseStorage targetDatabaseStorage;
        private readonly IState state;

        public ScriptEditor(ITargetDatabaseStorage targetDatabaseStorage, IState state)
        {
            this.targetDatabaseStorage = targetDatabaseStorage;
            this.state = state;
        }

        public string FixCreatingDatabaseScript(string creatingDatabaseScript, string sourceDatabaseName, string targetDatabaseName, string targetDatabaseFolderPath)
        {
            creatingDatabaseScript = ReplaceDatabaseFileSize(creatingDatabaseScript);
            creatingDatabaseScript = creatingDatabaseScript.Replace($"[{sourceDatabaseName}]", $"[{targetDatabaseName}]");
            creatingDatabaseScript = ReplaceDatabaseFileName(creatingDatabaseScript, targetDatabaseFolderPath);
            return creatingDatabaseScript;
        }

        public string FixCreatingScript(string creatingDatabaseObjectScript)
        {
            var replacedScript = ReplaceDatabaseName(creatingDatabaseObjectScript);
            return replacedScript
                .Replace("{", "{{")
                .Replace("}", "}}");
        }

        public string FixCreatingExtendedPropertyScript(string creatingExtendedPropertyScript)
        {
            var replacedScript = ReplaceDatabaseName(creatingExtendedPropertyScript);
            return replacedScript
                .Replace(Environment.NewLine, " ")
                .Replace("{", "{{")
                .Replace("}", "}}");
        }

        public string[] GetCreatingObjectScripts<T>(T creatingObject)
            where T : SqlSmoObject, IScriptable
        {
            var so = new ScriptingOptions {DriIndexes = true};
            var scripts = creatingObject.Script(so);

            var databaseInfo = DatabaseInfo.FromUrn(creatingObject.Urn);
            var targetDatabaseInfo = targetDatabaseStorage.GetTargetDatabaseInfo(databaseInfo.DatabaseName);

            var resultScripts = new List<string>();
            foreach (var script in scripts)
            {
                var database = creatingObject as Database;
                if (database != null)
                {
                    var fixedScript = FixCreatingDatabaseScript(
                        script,
                        databaseInfo.DatabaseName,
                        targetDatabaseInfo.DatabaseName,
                        targetDatabaseInfo.DatabaseFolderPath);
                    resultScripts.Add(fixedScript);
                }
                else if (creatingObject is ExtendedProperty)
                {
                    var fixedScript = FixCreatingExtendedPropertyScript(script);
                    resultScripts.Add(fixedScript);
                }
                else
                {
                    var fixedScript = FixCreatingScript(script);
                    resultScripts.Add(fixedScript);
                }
            }

            return resultScripts.ToArray();
        }

        private static string ReplaceDatabaseFileSize(string script)
        {
            const string pattern = @"SIZE = ([\S]*?) , MAXSIZE = ([\S]*?)\s?, FILEGROWTH = ([\S]*?)(\s|%)";

            var result = Regex.Replace(
                script,
                pattern,
                @"SIZE = 10000KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10000KB ");
            return result;
        }

        private static string ReplaceDatabaseFileName(string script, string folderPath)
        {
            var matches = Regex.Matches(script, @"FILENAME = N'([\s\S]*?)' ,");

            foreach (Match match in matches)
            {
                var matchedPath = match.Groups[1].Value;
                var fileName = Path.GetFileName(matchedPath);
                var path = Path.Combine(folderPath, $"{fileName}");
                script = script.Replace(matchedPath, path);
            }

            return script;
        }

        private string ReplaceDatabaseName(string script)
        {
            foreach (var createdDatabase in state.GetCreatedDatabaseNames())
            {
                var sDbName = createdDatabase.Key.DatabaseName;
                var sDbInstance = createdDatabase.Key.MsSqlInstance;
                var tDbName = createdDatabase.Value;

                script = ReplaceInOriginalAndLowerCase(script, $"[{sDbInstance}].[{sDbName}].", $"[{tDbName}].");
                script = ReplaceInOriginalAndLowerCase(script, $"[{sDbName}].", $"[{tDbName}].");

                script = ReplaceInOriginalAndLowerCase(script, $"{sDbInstance}.{sDbName}.", $"[{tDbName}].");
                script = ReplaceInOriginalAndLowerCase(script, $" {sDbName}.", $" [{tDbName}].");

                script = ReplaceInOriginalAndLowerCase(script, $"DATABASE [{sDbName}]", $"DATABASE [{tDbName}]");                
            }

            return script;
        }

        private string ReplaceInOriginalAndLowerCase(string value, string from, string to)
        {
            value = value.Replace(from, to);
            return value.Replace(from.ToLower(), to.ToLower());
        }
    }
}