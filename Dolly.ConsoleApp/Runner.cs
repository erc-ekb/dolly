﻿using System;
using System.Threading;
using Dolly.Abstractions;
using Dolly.ValueTypes;

namespace Dolly.ConsoleApp
{
    public class Runner
    {
        private const string DefaultDatabaseName = "GKU";
        private const string DefaultDatabaseFolder = "C:\\db";
        private static readonly string LocalhostServer = Environment.MachineName;
        private readonly IDollyLogger dollyLogger;

        public Runner(IDollyLogger dollyLogger)
        {
            this.dollyLogger = dollyLogger;
        }

        public void Main()
        {
            try
            {
                Console.Write($"Enter MS SQL instance with cloning database (default - {LocalhostServer}): ");
                var sourceMsSqlInstance = ReadValue(LocalhostServer);

                Console.Write($"Enter database name for cloning (default - {DefaultDatabaseName}): ");
                var cloningDatabaseName = ReadValue(DefaultDatabaseName);

                Console.Write($"Enter MS SQL instance clone to (default = {LocalhostServer}): ");
                var targetMsSqlInstance = ReadValue(LocalhostServer);

                Console.Write($"Enter database folder path (default - {DefaultDatabaseFolder}): ");
                var dbFolder = ReadValue(DefaultDatabaseFolder);

                var sourceDatabaseInfo = new DatabaseInfo(sourceMsSqlInstance, cloningDatabaseName);
                var targetDatabaseStorage = new LocalhostTargetDatabaseStorage(targetMsSqlInstance, dbFolder);

                var state = new DollyState();
                var cloningFactory = new CloningFactory(
                    dollyLogger,
                    targetDatabaseStorage,
                    new ScriptEditor(targetDatabaseStorage, state),
                    state
                );

                var databaseClone = cloningFactory.CloneDatabaseSchema(sourceDatabaseInfo);

                while (true)
                {
                    Thread.Sleep(1000);
                    Console.WriteLine();
                    Console.WriteLine("What do you want: ");
                    Console.WriteLine("0 - clone all");
                    Console.WriteLine("1 - clone tables");
                    Console.WriteLine("2 - clone stored procedures");
                    Console.WriteLine("3 - clone views");
                    Console.WriteLine("q - quit");
                    Console.Write("Command: ");
                    var command = Console.ReadLine();
                    if (command == "q")
                        return;

                    Console.Write("Enter object name with schema (if empty, clone all objects): ");
                    var objectName = ReadValue(null);
                    var schema = objectName != null && objectName.Contains(".")
                        ? objectName.Split(".")[0]
                        : "dbo";
                    DatabaseObjectInfo databaseObjectInfo = null;
                    if (objectName != null)
                        databaseObjectInfo = new DatabaseObjectInfo(schema, objectName);

                    switch (command)
                    {
                        case "0":
                            cloningFactory.CloneAllWithDependencies(sourceDatabaseInfo, databaseClone);
                            break;
                        case "1" when databaseObjectInfo == null:
                            cloningFactory.CloneAllTablesWithDependencies(sourceDatabaseInfo, databaseClone);
                            break;
                        case "1":
                            cloningFactory.CloneTableWithDependencies(sourceDatabaseInfo, databaseClone, databaseObjectInfo);
                            break;
                        case "2" when databaseObjectInfo == null:
                            cloningFactory.CloneAllStoredProceduresWithDependencies(sourceDatabaseInfo, databaseClone);
                            break;
                        case "2":
                            cloningFactory.CloneStoredProcedureWithDependencies(sourceDatabaseInfo, databaseClone, databaseObjectInfo);
                            break;
                        case "3" when databaseObjectInfo == null:
                            cloningFactory.CloneAllViewsWithDependencies(sourceDatabaseInfo, databaseClone);
                            break;
                        case "3":
                            cloningFactory.CloneViewWithDependencies(sourceDatabaseInfo, databaseClone, databaseObjectInfo);
                            break;
                        default:
                            Console.WriteLine($"Unknown command - {command}");
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
            }
        }

        private static string ReadValue(string defaultValue)
        {
            var val = Console.ReadLine();
            return string.IsNullOrEmpty(val) ? defaultValue : val;
        }
    }
}