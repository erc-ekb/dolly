﻿using System;
using Dolly.Abstractions;
using Dolly.Loggers.Nlog;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;

namespace Dolly.ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                var di = BuildDi();
                var runner = di.GetRequiredService<Runner>();
                runner.Main();

                Console.ReadKey();
            }
            catch (Exception e)
            {
                var logger = LogManager.GetCurrentClassLogger();
                logger.Error(e);
                Console.WriteLine(e);
                Console.ReadKey();
            }
        }

        private static ServiceProvider BuildDi()
        {
            return new ServiceCollection()
                .AddLogging(
                    builder =>
                    {
                        builder.SetMinimumLevel(LogLevel.Trace);
                        builder.AddNLog(
                            new NLogProviderOptions
                            {
                                CaptureMessageTemplates = true,
                                CaptureMessageProperties = true
                            });
                    })
                .AddTransient<Runner>()
                //.AddTransient<ICloningFactory, CloningFactory>()
                .AddTransient<IDollyLogger, NlogLogger>()
                //.AddTransient<ITargetDatabaseStorage, LocalhostTargetDatabaseStorage>()
                //.AddTransient<IScriptEditor, ScriptEditor>()
                .BuildServiceProvider();
        }
    }
}